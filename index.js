var http = require( 'http' );
var fs = require( 'fs' );
var indexHTML = fs.readFileSync( __dirname + '/index.html' );

var app = http.createServer( function( req, res ){
    
    console.log( 'Server started' );
    
    res.writeHead( 200, { 'Content-type': 'text/html' } );
    res.end( indexHTML );
    
} );

var io = require( 'socket.io' ).listen( app );

io.on( 'connection', function( socket ){
    
    io.sockets.sockets[ socket.id ].emit( 'assigned', socket.id );
    console.log( 'User: ' + socket.id + ' joined!' );
    
    socket.on( 'ping test', function( data ){
        
        io.sockets.sockets[ socket.id ].emit( 'ping return', data );
        
    } );
    
    socket.on( 'disconnect', function(){
        console.log( 'User: ' + socket.id + ' left!' );
    } );
} );

app.listen( 3000, function(){
	console.log( 'Listening 127.0.0.1:3000' );
} );