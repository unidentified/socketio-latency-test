# socketio-latency-test #


### What is this repository for? ###

Latency test for my raspi server. Easy to setup.

### How do I get set up? ###

Require Nodejs. Made using version 5.5.0.

1. go to project root on terminal.

2. npm install

3. nodejs index.js

4. connect to ip using port:3000